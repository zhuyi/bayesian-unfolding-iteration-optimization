#!/bin/bash

#setupATLAS
#lsetup cmake
#. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.02/x86_64-centos7-gcc48-opt/bin/thisroot.sh
#source /lustre/collider/wangzhen/software/RooUnfold/build/setup.sh
#source /lustre/collider/wangzhen/software/BootstrapGenerator/build/setup.sh

source /lustre/collider/zhuyifan/setup.sh
setupATLAS
lsetup git
lsetup cmake
. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.02/x86_64-centos7-gcc48-opt/bin/thisroot.sh
source /lustre/collider/liudanning/Zy+jets/Systematics/RooUnfold/build/setup.sh
source /lustre/collider/liudanning/Zy+jets/Systematics/BootstrapGenerator/build/setup.sh

export PATH="/lustre/collider/zhuyifan/VBSZZ/unfolding/iteration_opt/install/bin:${PATH}"
