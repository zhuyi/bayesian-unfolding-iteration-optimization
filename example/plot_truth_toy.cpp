void plot_truth_toy()
{
    gStyle->SetOptStat(0);

    TString symbol = "inclusive";
    TString var = "n_jets";
    TString path = symbol + "/" + var + "/bias/Bin_Info/";
    TString name = "Bin_10000toy_information_inclusive_" + var + ".root";

    auto c = new TCanvas("c", "", 800., 600.);

    auto file = new TFile(path + "/" + name, "read");
    for(auto k_obj : *file->GetListOfKeys())
    {
        auto *key = dynamic_cast<TKey*>(k_obj);
        auto hist = dynamic_cast<TH1D*>(key->ReadObj());

        auto hist_name = hist->GetName();
        auto match = TPRegexp("(bin_\\d+)").MatchS(hist_name);
        auto bin_name = dynamic_cast<TObjString*>(match->At(1))->GetString();
        bin_name.ReplaceAll("_", " ");

        double max = hist->GetMaximum();
        hist->Draw();
        hist->SetMaximum(1.2*max);
        hist->SetLineColor(kBlack);
        hist->SetLineWidth(2);
        hist->GetXaxis()->SetTitle("No. of events in " + var + " " + bin_name);
        hist->GetYaxis()->SetTitle("toys/bin");

        TLegend *legend = new TLegend(0.70, 0.83, 0.85, 0.82);
        legend->AddEntry(hist, bin_name, "L");
        legend->SetTextSize(0.03);
        legend->SetBorderSize(0);
        legend->Draw();

        auto text = new TPaveText(0.12, 0.81, 0.50, 0.89, "brNDC");
        text->SetFillColor(0);
        text->SetFillStyle(0);
        text->SetBorderSize(0);
        text->SetTextAlign(12);
        text->SetTextSize(0.03);
        text->AddText("#it{ATLAS} #bf{internal #sqrt{s}=13TeV 140fb^{-1}}");
        text->AddText("#bf{inclusive region, No. of jets}");
        text->Draw();

        auto plot_name = TString::Format("%s_%s.png", var.Data(), hist_name);
        c->SaveAs(plot_name);
    }
}
