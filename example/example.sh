#!/bin/bash

bias_path="/lustre/collider/zhuyifan/VBSZZ/unfolding/iteration_opt/run/test/bias"
stat_path="/lustre/collider/zhuyifan/VBSZZ/unfolding/iteration_opt/run/test/stat"
plot_path="/lustre/collider/zhuyifan/VBSZZ/unfolding/iteration_opt/run/test/plot"

eft_bias -n 10000 \
         -s Sherpa2211 \
         -inpf /lustre/collider/liudanning/EFTstudy/MakeLimitsInputs/results/UnfoldingInput/OneDimension/SM/Unfolded_Histograms_Sherpa2211.root \
         -outp ${bias_path} \
         -v yPt

eft_stat -s Sherpa2211 \
         -v yPt \
         -inpf /lustre/collider/liudanning/EFTstudy/MakeLimitsInputs/results/UnfoldingInput/OneDimension/SM/Unfolded_Histograms_Sherpa2211.root \
         -outp ${stat_path} \

eft_plot -inpbf ${bias_path}/Bias_Unfold/Bias_10000toy_unfolded_information_Sherpa2211_yPt.root  \
         -inpsf ${stat_path}/Plots/Bootstrap_Unfolded_Sherpa2211_yPt.root \
         -outp  ${plot_path} \
         -ib 1 \
         -v yPt \
         -s Sherpa2211
