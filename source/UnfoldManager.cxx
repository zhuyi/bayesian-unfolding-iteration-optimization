#include "UnfoldManager.h"

#include <iostream>
#include <fstream>

void UnfoldManager::GetHistogram(std::string input_file, std::string variable)
{
    std::cout << " [INFO]   : Starting to extract Histograms from " << input_file << std::endl;

    TFile * file = new TFile(input_file.c_str(), "READONLY");
    if (!file)
    {
        std::cout << " [ERROR]   : Open ROOT File Failed " << std::endl;
        exit(-1);
    }
    file->cd("nominal");

    std::string fid_name  = "effcorrden";
    std::string reco_name = "fidcorrden";
    std::string fcn_name  = "fidcorrnum";
    std::string fcd_name  = "fidcorrden";
    std::string ecn_name  = "effcorrnum";
    std::string ecd_name  = "effcorrden";
    std::string pur_name  = "puritynum";
    std::string sta_name  = "stabilityden";
    std::string matrix_name = "migration_matrix";

    auto GetTH1DFromInput = [&](std::string hist_name) -> TH1D*
                            {
                                auto temp = gDirectory->Get(hist_name.c_str());
                                if( dynamic_cast<TH1F*>(temp) )
                                {
                                    auto hist = new TH1D();
                                    dynamic_cast<TH1F*>(temp)->Copy(*hist);
                                    return hist;
                                }
                                else if( dynamic_cast<TH1D*>(temp) )
                                    return dynamic_cast<TH1D*>(temp);

                                return nullptr;
                            };

    hist_fid        = GetTH1DFromInput(fid_name);
    hist_reco       = GetTH1DFromInput(reco_name);
    hist_fidcorrnum = GetTH1DFromInput(fcn_name);
    hist_fidcorrden = GetTH1DFromInput(fcd_name);
    hist_effcorrnum = GetTH1DFromInput(ecn_name);
    hist_effcorrden = GetTH1DFromInput(ecd_name);
    hist_puritynum  = GetTH1DFromInput(pur_name);
    //hist_stabilityden = (TH1D*)gDirectory->Get(sta_name.c_str());
    hist_matrix     = (TH2D*)gDirectory->Get(matrix_name.c_str());

    std::cout << " [INFO]   : End of Histogram extraction " << std::endl;

    // file->Close();
}

TH1D* UnfoldManager::GetTruth()
{
    return hist_fid;
}

TH1D* UnfoldManager::GetMeasure()
{
    return hist_reco;
}

TH1D* UnfoldManager::GetFake()
{
    hist_fake = (TH1D*)hist_fidcorrden->Clone();
    hist_fake->Add(hist_fidcorrnum, -1);

    return hist_fake;
}

TH2D* UnfoldManager::GetMatrix()
{
    return hist_matrix;
}

RooUnfoldResponse UnfoldManager::GetResponse()
{
    // RooUnfoldResponse unf_res;
    // return unf_res;
    response.Setup(hist_reco, hist_fid, hist_matrix);

    return response;
}

TH1D* UnfoldManager::UnfoldBayes(TH1D * htemp, int nIteration)
{
    hist_fidcorr = (TH1D*)hist_fidcorrnum->Clone();
    hist_fidcorr->Divide(hist_fidcorrden);
    for (Int_t i = 1 ; i <= hist_fidcorr->GetNbinsX() ; i++)
    {
        hist_fidcorr->SetBinError(i, 0.);
    }
    // hist_fidcorr->Print("range");

    TH1D * tmp = (TH1D*)htemp->Clone();
    // tmp->Multiply(hist_fidcorr);
    // RooUnfoldBayes unfold (&response, tmp, nIteration, false, false);
    RooUnfoldBayes unfold (&response, tmp, nIteration);
    unfold.SetVerbose(-1);
    TH1D * hUnfold = (TH1D*)unfold.Hreco();

    return hUnfold;
}

TH1D* UnfoldManager::GenerateBootstrapSample(TH1D* hist_ori, TRandom3& rng)
{
    TH1D * hist_bootstrap = (TH1D*)hist_ori->Clone();
    hist_bootstrap->Reset();

    // Resampling with replacement
    for (int i = 1 ; i <= hist_ori->GetNbinsX() ; i++)
    {
        // Get the content of the randomly chosen bin
        double bin_content = hist_ori->GetBinContent(i);
        int sampleEvents = rng.Poisson(bin_content);
        
        // Fill the bootstrap histogram
        std::cout << " [INFO] : Bin number = " << i << " ; Bin content = " << bin_content << " ; Bin center = " << hist_ori->GetBinCenter(i) << std::endl;
        double bin_center = hist_ori->GetBinCenter(i);
        for (int j = 0 ; j < sampleEvents ; j++)
        {
            hist_bootstrap->Fill(bin_center);
        }
    }

    hist_bootstrap->Print("range");

    return hist_bootstrap;
}

UnfoldManager::~UnfoldManager()
{
    delete hist_fidcorr;
}
