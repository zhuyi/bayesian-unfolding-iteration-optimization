#include "UnfoldManager.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include <TSystem.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TStyle.h>
#include <iomanip>
#include <numeric>
#include <TRandom3.h>

#include "RooUnfoldResponse.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldBinByBin.h"

TRandom3 rnd(time(NULL));

int main(int argc, char *argv[])
{
    if (argc <= 1) { std::cout << " [ERROR] : No enough arguments " << std::endl; throw 1; }
    std::string variable;
    std::string symbol;
    std::string input_file;
    std::string output_path;
    int ntoys = 10000;
    for (int i = 1 ; i < argc ; i++)
    {
        if      (std::string(argv[i]) == "-v")      { variable      = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-s")      { symbol        = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-inpf")   { input_file    = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-outp")   { output_path   = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-ntoy")   { ntoys         = std::stoi(std::string(argv[i+1])); }
    }
    int hbin = 10;

    UnfoldManager * manager = new UnfoldManager();
    manager->GetHistogram(input_file, variable);
    TH1D * hfid  = manager->GetTruth();
    TH1D * hreco = manager->GetMeasure();
    TH1D * hfake = manager->GetFake();
    TH2D * hmatrix = manager->GetMatrix();
    RooUnfoldResponse _res = manager->GetResponse();
    hfake->Print("range");

    std::vector<TH1D*> hist_bias_true;
    std::vector<TH1D*> hist_bias_reco;
    std::vector<TH1D*> hist_iter1_unfold;
    std::vector<TH1D*> hist_iter2_unfold;
    std::vector<TH1D*> hist_iter3_unfold;
    std::vector<TH1D*> hist_iter4_unfold;
    std::vector<TH1D*> hist_iter5_unfold;
    std::vector<TH1D*> hist_iter7_unfold;
    std::vector<TH1D*> hist_iter10_unfold;
    std::vector<TH2D*> hist_iter1_bias;
    std::vector<TH2D*> hist_iter2_bias;
    std::vector<TH2D*> hist_iter3_bias;
    std::vector<TH2D*> hist_iter4_bias;
    std::vector<TH2D*> hist_iter5_bias;
    std::vector<TH2D*> hist_iter7_bias;
    std::vector<TH2D*> hist_iter10_bias;

    int nbins = 0;
    nbins = hfid->GetNbinsX();
    TH1D * ntoys_bin[nbins];
    for (int ibin = 1 ; ibin <= nbins ; ibin++)
    {
        double low_edge = 0;
        double high_edge = 0;
        double content = hfid->GetBinContent(ibin);
        low_edge  = content - 3.5*std::sqrt(content);
        high_edge = content + 3.5*std::sqrt(content);
        ntoys_bin[ibin-1] = new TH1D(Form("ntoy_bin_%i", ibin), " ", hbin, low_edge, high_edge);

        hist_bias_true.push_back(new TH1D("hist_bias_true_" + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_bias_reco.push_back(new TH1D("hist_bias_reco_" + TString(std::to_string(ibin).c_str()), " ", hbin, hreco->GetBinContent(ibin)-3.5*std::sqrt(hreco->GetBinContent(ibin)), hreco->GetBinContent(ibin)+3.5*std::sqrt(hreco->GetBinContent(ibin))));
        hist_iter1_unfold.push_back( new TH1D("hist_iter1_unfold_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_iter2_unfold.push_back( new TH1D("hist_iter2_unfold_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_iter3_unfold.push_back( new TH1D("hist_iter3_unfold_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_iter4_unfold.push_back( new TH1D("hist_iter4_unfold_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_iter5_unfold.push_back( new TH1D("hist_iter5_unfold_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_iter7_unfold.push_back( new TH1D("hist_iter7_unfold_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));
        hist_iter10_unfold.push_back(new TH1D("hist_iter10_unfold_" + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge));

        hist_iter1_bias.push_back( new TH2D("hist_iter1_bias_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
        hist_iter2_bias.push_back( new TH2D("hist_iter2_bias_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
        hist_iter3_bias.push_back( new TH2D("hist_iter3_bias_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
        hist_iter4_bias.push_back( new TH2D("hist_iter4_bias_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
        hist_iter5_bias.push_back( new TH2D("hist_iter5_bias_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
        hist_iter7_bias.push_back( new TH2D("hist_iter7_bias_"  + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
        hist_iter10_bias.push_back(new TH2D("hist_iter10_bias_" + TString(std::to_string(ibin).c_str()), " ", hbin, low_edge, high_edge, 50, -1, 1));
    }

    auto hist_ratios = [hfid](TH2D* _hbin, TH1D* _hunfolded, TH1D* htrue, int ibin)->void
    {
        double _ratio = (_hunfolded->GetBinContent(ibin+1) - htrue->GetBinContent(ibin+1)) / htrue->GetBinContent(ibin+1);
        _hbin->Fill(htrue->GetBinContent(ibin+1), _ratio);
    };

    for (int itoy = 0 ; itoy < ntoys ; itoy++)
    {
        if (itoy %1000 == 0) std::cout << " [INFO] : Performed " << itoy << " so far " << std::endl;
        TH1D * hist_toy_true = (TH1D*)hfid->Clone("hist_toy_true_" + TString(std::to_string(itoy).c_str()));
        hist_toy_true->Reset();

        for (int ibin = 1 ; ibin <= nbins ; ibin++)
        {
            hist_toy_true->SetBinContent(ibin, hfid->GetBinContent(ibin)+rnd.Gaus(0, 1.*std::sqrt(hfid->GetBinContent(ibin))));
            ntoys_bin[ibin-1]->Fill(hfid->GetBinContent(ibin)+rnd.Gaus(0, 1.*std::sqrt(hfid->GetBinContent(ibin))));
        }

        // hist_toy_true->Print("range");
        TH1D * hist_fold_reco = (TH1D*)hreco->Clone();
        hist_fold_reco->Reset();
        TH1D * hist_tmp_reco = (TH1D*)(_res.ApplyToTruth(hist_toy_true));
        for (int i = 1 ; i <= hist_fold_reco->GetNbinsX() ; i++)
        {
            hist_fold_reco->SetBinContent(i, hist_tmp_reco->GetBinContent(i));
        }
        hist_fold_reco->Add(hfake);

        // Now Unfold the toys with different iterations
        TH1D * hist_toy_unfold_01 = manager->UnfoldBayes(hist_fold_reco, 1);
        TH1D * hist_toy_unfold_02 = manager->UnfoldBayes(hist_fold_reco, 2);
        TH1D * hist_toy_unfold_03 = manager->UnfoldBayes(hist_fold_reco, 3);
        TH1D * hist_toy_unfold_04 = manager->UnfoldBayes(hist_fold_reco, 4);
        TH1D * hist_toy_unfold_05 = manager->UnfoldBayes(hist_fold_reco, 5);
        TH1D * hist_toy_unfold_07 = manager->UnfoldBayes(hist_fold_reco, 7);
        TH1D * hist_toy_unfold_10 = manager->UnfoldBayes(hist_fold_reco, 10);

        for (int ibin = 0 ; ibin < nbins ; ibin++)
        {
            hist_bias_true.at(ibin)    ->Fill(hist_toy_true ->GetBinContent(ibin+1));
            hist_bias_reco.at(ibin)    ->Fill(hist_fold_reco->GetBinContent(ibin+1));
            hist_iter1_unfold.at(ibin) ->Fill(hist_toy_unfold_01->GetBinContent(ibin+1));
            hist_iter2_unfold.at(ibin) ->Fill(hist_toy_unfold_02->GetBinContent(ibin+1));
            hist_iter3_unfold.at(ibin) ->Fill(hist_toy_unfold_03->GetBinContent(ibin+1));
            hist_iter4_unfold.at(ibin) ->Fill(hist_toy_unfold_04->GetBinContent(ibin+1));
            hist_iter5_unfold.at(ibin) ->Fill(hist_toy_unfold_05->GetBinContent(ibin+1));
            hist_iter7_unfold.at(ibin) ->Fill(hist_toy_unfold_07->GetBinContent(ibin+1));
            hist_iter10_unfold.at(ibin)->Fill(hist_toy_unfold_10->GetBinContent(ibin+1));
        }

        for (int ibin = 0 ; ibin < nbins ; ibin++)
        {
            hist_ratios(hist_iter1_bias.at(ibin),  hist_toy_unfold_01, hist_toy_true, ibin);
            hist_ratios(hist_iter2_bias.at(ibin),  hist_toy_unfold_02, hist_toy_true, ibin);
            hist_ratios(hist_iter3_bias.at(ibin),  hist_toy_unfold_03, hist_toy_true, ibin);
            hist_ratios(hist_iter4_bias.at(ibin),  hist_toy_unfold_04, hist_toy_true, ibin);
            hist_ratios(hist_iter5_bias.at(ibin),  hist_toy_unfold_05, hist_toy_true, ibin);
            hist_ratios(hist_iter7_bias.at(ibin),  hist_toy_unfold_07, hist_toy_true, ibin);
            hist_ratios(hist_iter10_bias.at(ibin), hist_toy_unfold_10, hist_toy_true, ibin);
        }
        delete hist_toy_true;
        delete hist_tmp_reco;
        delete hist_fold_reco;
        delete hist_toy_unfold_01;
        delete hist_toy_unfold_02;
        delete hist_toy_unfold_03;
        delete hist_toy_unfold_04;
        delete hist_toy_unfold_05;
        delete hist_toy_unfold_07;
        delete hist_toy_unfold_10;
    }

    gSystem->Exec(TString::Format("mkdir -vp %s", output_path.data()));
    gSystem->Exec(TString::Format("mkdir -vp %s/Bin_Info", output_path.data()));
    std::string ntoy_bin_filename = output_path + "/Bin_Info/Bin_" + std::to_string(ntoys) + "toy_information_" + symbol + "_" + variable + ".root";
    std::cout << " [INFO] : Output Bin Histogram File is : " << ntoy_bin_filename << std::endl;
    TFile * ntoy_bin_file = new TFile(ntoy_bin_filename.c_str(), "RECREATE");
    if (!ntoy_bin_file) std::cout << " [ERROR]  : Create ROOT File Failed, Please Check Output Path " << std::endl;

    ntoy_bin_file->cd();
    for (int ibin = 1 ; ibin <= nbins ; ibin++)
    {
        ntoys_bin[ibin-1]->SetName(Form("ntoy_bin_%i_information", ibin));
        ntoys_bin[ibin-1]->Write();
        ntoys_bin[ibin-1]->Delete();
    }
    ntoy_bin_file->Close();

    gSystem->Exec(TString::Format("mkdir -vp %s/Bias_Unfold", output_path.data()));
    std::string ntoy_unfold_filename = output_path + "/Bias_Unfold/Bias_" + std::to_string(ntoys) + "toy_unfolded_information_" + symbol + "_" + variable + ".root";
    std::cout << " [INFO] : Output Bias Unfolded Histogram File is : " << ntoy_unfold_filename << std::endl;
    TFile * ntoy_unfold_file = new TFile(ntoy_unfold_filename.c_str(), "RECREATE");
    if (!ntoy_unfold_file) std::cout << " [ERROR] : Create ROOT File Failed, Please Check Output Path " << std::endl;

    ntoy_unfold_file->cd();
    for (int ibin = 1 ; ibin <= nbins ; ibin++)
    {
        hist_bias_true[ibin-1]->Write();
        hist_bias_reco[ibin-1]->Write();

        hist_iter1_unfold[ibin-1]->Write();
        hist_iter2_unfold[ibin-1]->Write();
        hist_iter3_unfold[ibin-1]->Write();
        hist_iter4_unfold[ibin-1]->Write();
        hist_iter5_unfold[ibin-1]->Write();
        hist_iter7_unfold[ibin-1]->Write();
        hist_iter10_unfold[ibin-1]->Write();

        hist_iter1_bias[ibin-1]->Write();
        hist_iter2_bias[ibin-1]->Write();
        hist_iter3_bias[ibin-1]->Write();
        hist_iter4_bias[ibin-1]->Write();
        hist_iter5_bias[ibin-1]->Write();
        hist_iter7_bias[ibin-1]->Write();
        hist_iter10_bias[ibin-1]->Write();
    } 
    ntoy_unfold_file->Close(); 

    hist_bias_true.clear();
    hist_bias_reco.clear();
    hist_iter1_unfold.clear();
    hist_iter2_unfold.clear();
    hist_iter3_unfold.clear();
    hist_iter4_unfold.clear();
    hist_iter5_unfold.clear();
    hist_iter7_unfold.clear();
    hist_iter10_unfold.clear();
    hist_iter1_bias.clear();
    hist_iter2_bias.clear();
    hist_iter3_bias.clear();
    hist_iter4_bias.clear();
    hist_iter5_bias.clear();
    hist_iter7_bias.clear();
    hist_iter10_bias.clear();

    // Statistic Uncertainty 

    return 0;
}
