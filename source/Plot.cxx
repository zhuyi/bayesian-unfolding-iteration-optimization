#include "UnfoldManager.h"

#include <iostream>
#include <fstream>

#include <TSystem.h>

int main(int argc, char *argv[])
{
    TLatex atlaslabel;
    atlaslabel.SetNDC();
    atlaslabel.SetTextFont(72);
    atlaslabel.SetTextSize(0.03);
    atlaslabel.SetTextColor(1);
    TLatex atlastext;
    atlastext.SetNDC();
    atlastext.SetTextColor(1);
    atlastext.SetTextSize(0.03);

    std::map<std::string, std::string> hist_xtitles = { {"yPt",  "p_{T}^{#gamma} [GeV]"},
                                                        {"dRll", "#Delta R(l,l)"},
                                                        {"mZ",   "m_{Z} [GeV]"},
                                                        {"mZy",  "m_{Z}^{#gamma} [GeV]"},
                                                        {"ptZ",  "p_{T}^{Z} [GeV]"},
                                                        {"ptZy", "p_{T}^{Z#gamma} [GeV]"},
                                                        {"ptZy2mZy", "p_{T}^{Z#gamma}/m_{Z#gamma}"},
                                                        {"cosphi", "cos#phi"},
                                                        {"phi",  "#phi [rad]"},
                                                        {"mtZ",  "m_{T}^{Z} [GeV]"},
                                                        {"mtZy", "m_{T}^{Z#gamma} [GeV]"},
                                                        {"n_jets", "No. of jets"},
                                                        {"leading_jet_pt", "p_{T}^{leading jet} [GeV]"},
                                                        {"ptz",  "p_{T}^{Z} [GeV]"},
                                                        {"pt_zz", "p_{T}^{ZZ} [GeV]"},
                                                        {"mt_zz", "m_{T}^{ZZ} [GeV]"},
                                                        {"leading_pT_lepton", "p_{T}^{leading lepton} [GeV]"},
                                                        {"Z_rapidity", "y(Z)"},
                                                        {"dphill", "#Delta#Phi(l,l)"},
                                                        {"mjj", "m_{jj} [GeV]"},
                                                       };
                                                        

    if (argc <= 1) { std::cout << " [ERROR] : No enough arguments " << std::endl; throw 1; }
    std::string variable;
    std::string symbol;
    std::string input_bias_file;
    std::string input_stat_file;
    std::string output_path;
    std::string index_bin;
    for (int i = 1 ; i < argc ; i++)
    {
        if      (std::string(argv[i]) == "-v")     { variable    = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-s")     { symbol      = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-inpsf") { input_stat_file  = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-inpbf") { input_bias_file = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-outp")  { output_path = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-ib")    { index_bin   = std::string(argv[i+1]); }
    }
    std::string xtitle = variable;
    if(hist_xtitles.count(variable)) xtitle = hist_xtitles.at(variable);
    std::string unit = " [GeV]";
    auto legend_title = xtitle;
    if(legend_title.find(unit) != std::string::npos) legend_title.replace(legend_title.find(unit), sizeof(unit) - 1, "");
    
    TFile * finput = new TFile(input_bias_file.c_str(), "READONLY");
    if (!finput)
    {
        std::cout << " [ERROR] : Open Histogram Root File Failed " << std::endl;
        exit(-1);
    }

    std::vector<std::string> iterations = {"1", "2", "3", "4", "5", "7", "10"};
    int Isize = iterations.size();

    std::string bin_info = "Bin " + index_bin;
    std::vector<int> colors = {kBlack, kGreen+1, kCyan+2, kBlue+1, kMagenta+1, kOrange};

    TH1D * hists[Isize];
    for (int it = 0 ; it < Isize ; it++)
    {
        std::string histname = "hist_iter" + iterations[it] + "_unfold_" + index_bin;
        std::cout << " [INFO]  : Taking Unfolded Histogram with " << iterations[it] << " iterations" << std::endl;

        hists[it] = (TH1D*)finput->Get(histname.c_str());
        if (!hists[it])
        {
            std::cout << " [INFO]  : Taking Unfolded Histogram Failed " << std::endl;
            exit(-1);
        }

        hists[it]->SetLineWidth(2);
        hists[it]->SetLineColor(colors[it]);
    }
    double xmin_unfold = hists[0]->GetXaxis()->GetXmin();
    double xmax_unfold = hists[0]->GetXaxis()->GetXmax();
    double ymin_unfold = hists[0]->GetMinimum();
    double ymax_unfold = hists[0]->GetMaximum();

    TH2D * unfold_template = new TH2D("unfold_template", " ", 10, xmin_unfold, xmax_unfold, 10, ymin_unfold*0.1, ymax_unfold*1.3);
    std::string unfold_yt = "Events/bin";
    // Set2DTemplate(unfold_template, xtitle, unfold_yt);
    unfold_template->GetXaxis()->SetTitle(xtitle.c_str());
    unfold_template->GetXaxis()->SetTitleSize(0.04);
    unfold_template->GetXaxis()->SetTitleOffset(1.0);
    unfold_template->GetXaxis()->SetLabelSize(0.03);
    unfold_template->GetXaxis()->SetLabelOffset(0.01);

    unfold_template->GetYaxis()->SetTitle(unfold_yt.c_str());
    unfold_template->GetYaxis()->SetTitleSize(0.04);
    unfold_template->GetYaxis()->SetTitleOffset(1.2);
    unfold_template->GetYaxis()->SetLabelSize(0.03);
    unfold_template->GetYaxis()->SetLabelOffset(0.01);

    TCanvas * can_unfold = new TCanvas("can_unfold", "canvas", 800, 700);
    gStyle->SetOptStat(0000);
    TPad * pad_unfold = new TPad("pad_unfold", "pad", 0.01, 0.01, 0.99, 0.99);
    pad_unfold->SetMargin(0.1, 0.05, 0.1, 0.05);
    pad_unfold->Draw();

    TLegend * leg_unfold = new TLegend(0.65, 0.6, 0.9, 0.9);
    for (int it = 0 ; it < Isize ; it++)
    {
        std::string leg_name = "Iteration " + iterations[it];
        leg_unfold->AddEntry(hists[it], leg_name.c_str(), "pl");
    }
    leg_unfold->SetLineColor(0);
    leg_unfold->SetFillColor(0);

    TText * text_unfold = new TText(hists[0]->GetBinCenter(hists[0]->GetNbinsX()-2), ymax_unfold*0.2, bin_info.c_str());
    text_unfold->SetTextSize(0.05);

    can_unfold->cd();
    pad_unfold->cd();
    unfold_template->Draw();
    leg_unfold->Draw("same");
    text_unfold->Draw("same");
    for (int it = 0 ; it < Isize ; it++)
    {
        hists[it]->Draw("same hist e1");
    }
    atlaslabel.DrawLatex(0.15, 0.85, "ATLAS Internal");
    atlastext.DrawLatex(0.15, 0.80,  "#sqrt{s} = 13TeV, 140 fb^{-1}");
    std::string canname_unfold = output_path + "/Iterations_Bin/Iterations_10000toy_Bin" + index_bin + "_" + symbol + "_" + variable + ".pdf"; 
    gSystem->Exec(TString::Format("mkdir -vp %s", output_path.data()));
    gSystem->Exec(TString::Format("mkdir -vp %s/Iterations_Bin", output_path.data()));
    can_unfold->SaveAs(canname_unfold.c_str());

    // ================================= 
    // ======== Difference Plot ========
    // =================================

    TH1D * ratios[Isize];
    TH1D * hist_diff[Isize];
    std::string truth_toyname = "hist_bias_true_" + index_bin;
    TH1D * hist_truthtoy = (TH1D*)finput->Get(truth_toyname.c_str());
    for (int it = 0 ; it < Isize ; it++)
    {
        std::string histname = "hist_iter" + iterations[it] + "_unfold_" + index_bin;

        TH1D * htmp = (TH1D*)finput->Get(histname.c_str());
        if (!htmp) std::cout << " [INFO]  : Taking Unfolded Histogram Failed " << std::endl;  
        
        hist_diff[it] = (TH1D*)htmp->Clone();
        hist_diff[it]->Add(hist_truthtoy, -1);

        ratios[it] =(TH1D*)hist_diff[it]->Clone();
        ratios[it]->Divide(hist_truthtoy); 
        ratios[it]->SetLineWidth(2);
        ratios[it]->SetLineColor(colors[it]); 
    }

    double xmin_diff = ratios[0]->GetXaxis()->GetXmin();
    double xmax_diff = ratios[0]->GetXaxis()->GetXmax();
    double ymin_diff = -1;
    double ymax_diff = 1;
    // double ymin_diff = ratios[0]->GetMinimum();
    // double ymax_diff = ratios[0]->GetMaximum();
    // if (ymin_diff < 0 && ymax_diff > 0) { ymin_diff *= 2; ymin_diff *= 2; }
    // else { ymin_diff = -0.2;  ymax_diff = 0.2; }

    TH2D * diff_template = new TH2D("diff_template", " ", 10, xmin_diff, xmax_diff, 10, ymin_diff, ymax_diff);
    std::string diff_yt = "(unfolded-true)/true";
    diff_template->GetXaxis()->SetTitle(xtitle.c_str());
    diff_template->GetXaxis()->SetTitleSize(0.04);
    diff_template->GetXaxis()->SetTitleOffset(1.0);
    diff_template->GetXaxis()->SetLabelSize(0.03);
    diff_template->GetXaxis()->SetLabelOffset(0.01);

    diff_template->GetYaxis()->SetTitle(diff_yt.c_str());
    diff_template->GetYaxis()->SetTitleSize(0.04);
    diff_template->GetYaxis()->SetTitleOffset(1.2);
    diff_template->GetYaxis()->SetLabelSize(0.03);
    diff_template->GetYaxis()->SetLabelOffset(0.01);

    TCanvas * can_diff = new TCanvas("can_diff", "canvas", 800, 700);
    gStyle->SetOptStat(0000);
    TPad * pad_diff = new TPad("pad_diff", "pad", 0.01, 0.01, 0.99, 0.99);
    pad_diff->SetMargin(0.1, 0.05, 0.1, 0.05);
    pad_diff->Draw();

    TLegend * leg_diff = new TLegend(0.65, 0.6, 0.9, 0.9);
    for (int it = 0 ; it < Isize ; it++)
    {
        std::string leg_name = "Iteration " + iterations[it];
        leg_diff->AddEntry(hists[it], leg_name.c_str(), "pl");
    }
    leg_diff->SetLineColor(0);
    leg_diff->SetFillColor(0);

    TText * text_diff = new TText(ratios[0]->GetBinCenter(2), -0.95, bin_info.c_str());
    text_diff->SetTextSize(0.05);

    can_diff->cd();
    pad_diff->cd();
    diff_template->Draw();
    leg_diff->Draw("same");
    text_diff->Draw("same");
    for (int it = 0 ; it < Isize ; it++)
    {
        ratios[it]->Draw("same hist");
    }
    atlaslabel.DrawLatex(0.15, 0.85, "ATLAS Internal");
    atlastext.DrawLatex(0.15, 0.80,  "#sqrt{s} = 13TeV, 140 fb^{-1}");
    std::string canname_diff = output_path + "/Iterations_Bin/Ratios_Iterations_10000toy_Bin" + index_bin + "_" + symbol + "_" + variable + ".pdf"; 
    can_diff->SaveAs(canname_diff.c_str());

    TGraph * gbias = new TGraph();
    TGraph * gstat = new TGraph();
    TGraph * gcombine = new TGraph();
    TGraph * gberr = new TGraph();

    TFile * fstat_input = TFile::Open(input_stat_file.c_str(), "READONLY");
    if (!fstat_input)
    {
        std::cout << " [ERROR] : Can not open Statistic Bootstrap Root File " << std::endl;
        exit(-1);
    }
    TH1D * hstats[Isize];
    TH1D * hberrs[Isize];

    double ymax_graph=0;

    for (int it = 0 ; it < Isize ; it++)
    {
        // std::string bias_name = "hist_iter" + iterations[it] + "_bias_" + index_bin;
        // std::cout << " [INFO]  : Taking Biased Histogram " << bias_name << std::endl;
        // TH2D * rbias = (TH2D*)finput->Get(bias_name.c_str());

        // if (!rbias) std::cout << " [ERROR] : Taking bias histogram Failed " << std::endl;
        // TH1D * hbias = (TH1D*)rbias->ProjectionX();

        std::string bias_name = "hist_iter" + iterations[it] + "_unfold_" + index_bin;
        std::cout << " [INFO]  : Taking Biased Histogram " << bias_name << std::endl;
        TH1D * rbias = (TH1D*)finput->Get(bias_name.c_str());
        if (!rbias)
        {
            std::cout << " [ERROR] : Taking bias histogram Failed " << std::endl;
            continue;
        }
        TH1D * hbias = (TH1D*)rbias->Clone();

        hbias->SetDirectory(0);

        double bias = 0;
        double bias_value = 0;
        double true_value = 0;
        // bias method 1 average bias value
        double xmiddle = 0;
        // bias method 2 : 68% value 
        xmiddle = (hbias->GetBinLowEdge(1) + hbias->GetBinLowEdge(hbias->GetNbinsX()) + hbias->GetBinWidth(hbias->GetNbinsX())) / 2.;
        // bias = std::fabs(hbias->GetBinContent(hbias->FindBin(xmiddle - std::sqrt(xmiddle))));

        bias_value = std::fabs(hbias->GetBinContent(hbias->FindBin(xmiddle - std::sqrt(xmiddle))));
        true_value = std::fabs(hist_truthtoy->GetBinContent(hist_truthtoy->FindBin(xmiddle - std::sqrt(xmiddle))));
        bias = std::fabs(bias_value - true_value)/true_value;

        std::cout << " [INFO]  : middle value = " << xmiddle << " || bias value = " << bias << std::endl;
        gbias->SetPoint(it, std::stod(iterations[it]), bias);

        std::string stat_name = "herr_" + iterations[it];
        std::cout << " [INFO]  : Taking Stat Histogram " << stat_name << std::endl;
        hstats[it] = (TH1D*)fstat_input->Get(stat_name.c_str());
        hstats[it]->SetDirectory(0);

        std::string berr_name = "hberr_" + iterations[it];
        hberrs[it] = (TH1D*)fstat_input->Get(berr_name.c_str());
        hberrs[it]->SetDirectory(0);
        if (!hstats[it] || !hberrs[it])
        {
            std::cout << " [ERROR] : Taking stat histogram Failed " << std::endl;
            continue;
        }

        auto stat_err = hstats[it]->GetBinContent(std::stoi(index_bin));
        gstat->SetPoint(it, std::stod(iterations[it]), stat_err);

        auto total_err = std::sqrt(TMath::Power(bias, 2) + TMath::Power(stat_err, 2));
        gcombine->SetPoint(it, std::stod(iterations[it]), total_err);

        auto berr = hberrs[it]->GetBinContent(std::stoi(index_bin));
        gberr->SetPoint(it, std::stod(iterations[it]), berr);

        if (it == 0) 
        {
            ymax_graph = total_err * 2.;
            if(ymax_graph < 0.09) ymax_graph = 0.09;
        }

        std::cout << " [DEBUG] : bias_err = " << bias << " stat_err = " << stat_err << " total_err = " << total_err << std::endl;
    }

    gstat->SetLineWidth(2);
    gbias->SetLineWidth(2);
    gcombine->SetLineWidth(2);
    gberr->SetLineWidth(2);

    gstat->SetLineColor(9);
    gbias->SetLineColor(30);
    gcombine->SetLineColor(6);
    gberr->SetLineColor(kOrange);

    gstat->SetMarkerStyle(kFullCircle);
    gbias->SetMarkerStyle(kFullCircle);
    gcombine->SetMarkerStyle(kFullCircle);
    gberr->SetMarkerStyle(kFullCircle);

    gstat->SetMarkerSize(0.5);
    gbias->SetMarkerSize(0.5);
    gcombine->SetMarkerSize(0.5);
    gberr->SetMarkerSize(0.5);

    gstat->SetMarkerColor(9);
    gbias->SetMarkerColor(30);
    gcombine->SetMarkerColor(6);
    gberr->SetMarkerColor(kOrange);

    gberr->SetLineStyle(kDashed);

    TCanvas * can = new TCanvas("can", "can", 800, 700);
    gStyle->SetOptStat(0000);
    TPad * pad = new TPad("pad", "pad", 0.01, 0.01, 0.99, 0.99);
    pad->SetMargin(0.1, 0.05, 0.1, 0.05);
    pad->Draw();
    TH2D * htmp_graph = new TH2D("htmp_graph", " ", 10, 0, 11, 10, -0.01, ymax_graph);
    htmp_graph->GetXaxis()->SetTitle("Niterations");
    htmp_graph->GetXaxis()->SetTitleSize(0.04);
    htmp_graph->GetXaxis()->SetTitleOffset(1.0);
    htmp_graph->GetXaxis()->SetLabelSize(0.03);
    htmp_graph->GetXaxis()->SetLabelOffset(0.01);
    htmp_graph->GetYaxis()->SetTitle("Relative Uncertainty");
    htmp_graph->GetYaxis()->SetTitleSize(0.04);
    htmp_graph->GetYaxis()->SetTitleOffset(1.2);
    htmp_graph->GetYaxis()->SetLabelSize(0.03);
    htmp_graph->GetYaxis()->SetLabelOffset(0.01);

    std::vector<std::string> graph_legends = {"Bias", "Stat Unc.", "Total Unc.", "Stat Unc. before Unfolding"};
    TLegend * leg_graph = new TLegend(0.65, 0.75, 0.9, 0.9);
    leg_graph->AddEntry(gbias,    graph_legends[0].c_str(), "pl");
    leg_graph->AddEntry(gstat,    graph_legends[1].c_str(), "pl");
    leg_graph->AddEntry(gcombine, graph_legends[2].c_str(), "pl");
    leg_graph->AddEntry(gberr,    graph_legends[3].c_str(), "pl");
    leg_graph->SetLineColor(0);
    leg_graph->SetFillColor(0);

    TText * text_graph = new TText(1, 0.0, bin_info.c_str());
    text_graph->SetTextSize(0.03);
    can->cd();
    pad->cd();
    htmp_graph->Draw();
    leg_graph->Draw("same");
    text_graph->Draw("same");
    gbias->Draw("same PL");
    gstat->Draw("same PL");
    gcombine->Draw("same PL");
    gberr->Draw("same PL");
    atlaslabel.DrawLatex(0.15, 0.85, "ATLAS Internal");
    atlastext.DrawLatex(0.15, 0.77,  ("#splitline{#sqrt{s} = 13TeV, 140 fb^{-1}}{" + (symbol == "inclusive" ? std::string("inclusive") : std::string("ZZjj")) + " region, " + legend_title + "}").data());
    std::string canname_graph = output_path + "/Bias_graph/Relative_Uncertainty_Bin" + index_bin + "_" + symbol + "_" + variable; 
    gSystem->Exec(TString::Format("mkdir -vp %s/Bias_graph", output_path.data()));
    std::cout << " [INFO]  : Output PDF File is : " << canname_graph << std::endl;
    can->SaveAs((canname_graph + ".pdf").c_str());
    can->SaveAs((canname_graph + ".png").c_str());
    
    return 0;
}
