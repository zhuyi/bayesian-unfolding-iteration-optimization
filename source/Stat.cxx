#include "UnfoldManager.h"

#include <iostream>
#include <fstream>
#include <cstdio>

#include <vector>
#include <random>
#include <algorithm>

#include <TSystem.h>
#include <TH2D.h>
#include <TRandom3.h>
#include <TVector3.h>
#include <TFile.h>
#include <TStopwatch.h>
#include <TSVDUnfold.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <BootstrapGenerator/BootstrapGenerator.h>
#include <BootstrapGenerator/TH1DBootstrap.h>
#include <BootstrapGenerator/TH2DBootstrap.h>

int main(int argc, char *argv[])
{
    if (argc <= 1) { std::cout << " [ERROR] : No enough arguments " << std::endl; throw 1; }
    std::string variable;
    std::string symbol;
    std::string input_file;
    std::string output_path;
    for (int i = 1 ; i < argc ; i++)
    {
        if      (std::string(argv[i]) == "-v")      { variable      = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-s")      { symbol        = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-inpf")   { input_file    = std::string(argv[i+1]); }
        else if (std::string(argv[i]) == "-outp")   { output_path   = std::string(argv[i+1]); }
    }
    
    UnfoldManager * manager = new UnfoldManager();
    manager->GetHistogram(input_file, variable);
    TH1D * hfid  = manager->GetTruth();
    TH1D * hreco = manager->GetMeasure();
    TH1D * hfake = manager->GetFake();
    TH2D * hmatrix = manager->GetMatrix();
    RooUnfoldResponse _res = manager->GetResponse();
    hreco->Print("range");

    // number of boostrap samples we want to generate;
    int nrep = 1000;

    // Vector to hold bootstrap samples
    std::vector<TH1D*> bootstrap_samples;

    // Random number generator
    TRandom3 rng(0);    // Seed with 0, which uses the machine's clock

    for (int i = 0 ; i < nrep ; i++)
    {
        bootstrap_samples.push_back(manager->GenerateBootstrapSample(hreco, rng));
    }

    // At this point, 'bootstrap_samples" contains your bootstrap samples
    // You can now proceed with your analysis using these samples

    std::vector<TH1D*> vec_h1,  vec_h2,  vec_h3,  vec_h4,  vec_h5,  vec_h7,  vec_h10;
    std::vector<TH1D*> vec_bh1, vec_bh2, vec_bh3, vec_bh4, vec_bh5, vec_bh7, vec_bh10;

    for (int irep = 0 ; irep < nrep ; irep++)
    {
        TH1D * htmp_reco = (TH1D*)bootstrap_samples[irep]->Clone(); 
        TH1D * hb1 = (TH1D*)htmp_reco->Clone("hb1_toy" + TString(std::to_string(irep)));
        TH1D * hb2 = (TH1D*)htmp_reco->Clone("hb2_toy" + TString(std::to_string(irep)));
        TH1D * hb3 = (TH1D*)htmp_reco->Clone("hb3_toy" + TString(std::to_string(irep)));
        TH1D * hb4 = (TH1D*)htmp_reco->Clone("hb4_toy" + TString(std::to_string(irep)));
        TH1D * hb5 = (TH1D*)htmp_reco->Clone("hb5_toy" + TString(std::to_string(irep)));
        TH1D * hb7 = (TH1D*)htmp_reco->Clone("hb7_toy" + TString(std::to_string(irep)));
        TH1D * hb10= (TH1D*)htmp_reco->Clone("hb10_toy"+ TString(std::to_string(irep)));
        TH1D * hu1 = (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco, 1)->Clone("hu1_toy" + TString(std::to_string(irep)));
        TH1D * hu2 = (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco, 2)->Clone("hu2_toy" + TString(std::to_string(irep)));
        TH1D * hu3 = (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco, 3)->Clone("hu3_toy" + TString(std::to_string(irep)));
        TH1D * hu4 = (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco, 4)->Clone("hu4_toy" + TString(std::to_string(irep)));
        TH1D * hu5 = (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco, 5)->Clone("hu5_toy" + TString(std::to_string(irep)));
        TH1D * hu7 = (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco, 7)->Clone("hu7_toy" + TString(std::to_string(irep)));
        TH1D * hu10= (TH1D*)(TH1D*)manager->UnfoldBayes(htmp_reco,10)->Clone("hu10_toy"+ TString(std::to_string(irep)));

        vec_h1.push_back(hu1);
        vec_h2.push_back(hu2);
        vec_h3.push_back(hu3);
        vec_h4.push_back(hu4);
        vec_h5.push_back(hu5);
        vec_h7.push_back(hu7);
        vec_h10.push_back(hu10);
        vec_bh1.push_back(hb1);
        vec_bh2.push_back(hb2);
        vec_bh3.push_back(hb3);
        vec_bh4.push_back(hb4);
        vec_bh5.push_back(hb5);
        vec_bh7.push_back(hb7);
        vec_bh10.push_back(hb10);
    }
    std::cout << " [INFO] :  Finish Bootstrap Generation and Unfold " << std::endl;

    auto f = [&manager](std::vector<TH1D*> htmp, TH1D* hn, int niter, bool isunfold, std::string name)->TH1D*
    {
        TH1D * hs = (TH1D*)hn->Clone(TString(name) + TString(std::to_string(niter)));
        hs->Reset();
        // TH1D * hnominal = manager->UnfoldBayes(hs, niter);
        int nbins = htmp[0]->GetNbinsX();
        TH1D * hbin_index[nbins];
        for (int ibin = 1 ; ibin <= nbins ; ibin++)
        {
            std::string hname_index = "hbin_" + std::to_string(ibin) + "_" + std::to_string(niter);
            hbin_index[ibin-1] = new TH1D(hname_index.c_str(), " ", 1000, 0, 1000);
        }
        for (TH1D* ihist : htmp)
        {
            for (int ibin = 1 ; ibin <= nbins ; ibin++)
            {
                hbin_index[ibin-1]->Fill(ihist->GetBinContent(ibin));
            }
        }
        for (int ibin = 1 ; ibin <= nbins ; ibin++)
        {
            if( hbin_index[ibin-1]->GetMean() != 0. )
                hs->SetBinContent(ibin, hbin_index[ibin-1]->GetRMS()/hbin_index[ibin-1]->GetMean());
            else hs->SetBinContent(ibin, 0.);
            hs->SetBinError(ibin, 0);
        }
        hs->Print("range");

        for (int ibin = 1 ; ibin <= nbins ; ibin++)
        {
            delete hbin_index[ibin-1];
        }

        return hs;
    };

    std::cout << " [INFO] :  Finish Histogram Reset " << std::endl;
    TH1D * hs1  = (TH1D*)f(vec_h1, hreco, 1, true, "herr_");
    TH1D * hs2  = (TH1D*)f(vec_h2, hreco, 2, true, "herr_");
    TH1D * hs3  = (TH1D*)f(vec_h3, hreco, 3, true, "herr_");
    TH1D * hs4  = (TH1D*)f(vec_h4, hreco, 4, true, "herr_");
    TH1D * hs5  = (TH1D*)f(vec_h5, hreco, 5, true, "herr_");
    TH1D * hs7  = (TH1D*)f(vec_h7, hreco, 7, true, "herr_");
    TH1D * hs10 = (TH1D*)f(vec_h10,hreco,10, true, "herr_");
    TH1D * hbs1 = (TH1D*)f(vec_bh1, hreco, 1, false, "hberr_");
    TH1D * hbs2 = (TH1D*)f(vec_bh2, hreco, 2, false, "hberr_");
    TH1D * hbs3 = (TH1D*)f(vec_bh3, hreco, 3, false, "hberr_");
    TH1D * hbs4 = (TH1D*)f(vec_bh4, hreco, 4, false, "hberr_");
    TH1D * hbs5 = (TH1D*)f(vec_bh5, hreco, 5, false, "hberr_");
    TH1D * hbs7 = (TH1D*)f(vec_bh7, hreco, 7, false, "hberr_");
    TH1D * hbs10= (TH1D*)f(vec_bh10,hreco, 10,false, "hberr_");

    gSystem->Exec(TString::Format("mkdir -vp %s", output_path.data()));
    gSystem->Exec(TString::Format("mkdir -vp %s/Plots", output_path.data()));
    std::string output_filename = output_path + "/Plots/Bootstrap_Unfolded_" + symbol + "_" + variable + ".root";
    TFile * output_file = new TFile(output_filename.c_str(), "RECREATE");
    if (!output_file) std::cout << " [ERROR] : CREATE ROOT File Failed " << std::endl;
    std::cout << " [INFO] :  Create : " << output_filename << " Succeed " << std::endl;

    output_file->cd();
    hs1->Write();
    hs2->Write();
    hs3->Write();
    hs4->Write();
    hs5->Write();
    hs7->Write();
    hs10->Write();
    hbs1->Write();
    hbs2->Write();
    hbs3->Write();
    hbs4->Write();
    hbs5->Write();
    hbs7->Write();
    hbs10->Write();

    // Clean up : Don't forget to delete the histograms once you're done to avoid memory leaks
    for (TH1D* hist : bootstrap_samples)
    {
        delete hist;
    }
    vec_h1.clear();
    vec_h2.clear();
    vec_h3.clear();
    vec_h4.clear();
    vec_h5.clear();
    vec_h7.clear();
    vec_h10.clear();
    vec_bh1.clear();
    vec_bh2.clear();
    vec_bh3.clear();
    vec_bh4.clear();
    vec_bh5.clear();
    vec_bh7.clear();
    vec_bh10.clear();

    delete hs1;
    delete hs2;
    delete hs3;
    delete hs4;
    delete hs5;
    delete hs7;
    delete hs10;
    delete hbs1;
    delete hbs2;
    delete hbs3;
    delete hbs4;
    delete hbs5;
    delete hbs7;
    delete hbs10;

    return 0;
}
