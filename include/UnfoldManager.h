#ifndef _UNFOLD_MANAGER_H_
#define _UNFOLD_MANAGER_H_

#include <TRandom3.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <numeric>
#include <TH1.h>
#include <TH2.h>
#include <TObject.h>
#include <TProfile.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>
#include <TFile.h>
#include <TList.h>
#include <TDirectory.h>
#include <TRandom.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TTree.h>
#include <TStyle.h>
#include <TMatrixD.h>
#include <TVectorD.h>
#include <TText.h>
#include <TLine.h>
#include <TFile.h>
#include <TROOT.h> 
#include <TPad.h>
#include <TMath.h>
#include <TLatex.h>
#include <TPave.h>
#include <TMarker.h>
#include <TRandom3.h>

#include <RooUnfoldResponse.h>
#include <RooUnfoldBayes.h>
#include <RooUnfoldBinByBin.h>
#include <TSVDUnfold.h>
#include <BootstrapGenerator/TH1DBootstrap.h>

class UnfoldManager
{
    public:
        UnfoldManager(){};
        ~UnfoldManager();

        TH1D* GetTruth();
        TH1D* GetMeasure();
        TH1D* GetFake();
        TH2D* GetMatrix();

        RooUnfoldResponse GetResponse();

        void GetHistogram(std::string input_file, std::string variable);
        void SetFidCorr();
    
        TH1D* UnfoldBayes(TH1D * htemp, int nIteration);
        // TH1D* Fold(TH1D* hist_tobe_unfold, TH1D* measure, TH1D* truth, TU2D* matrix, int nIteration);

        TH1D* GenerateBootstrapSample(TH1D* hist_ori, TRandom3& rng);

    private:
        
        TH1D * hist_fid;
        TH1D * hist_reco;
        TH1D * hist_fidcorrnum;
        TH1D * hist_fidcorrden;
        TH1D * hist_effcorrnum;
        TH1D * hist_effcorrden;
        TH1D * hist_puritynum;
        TH1D * hist_stabilityden;
        TH2D * hist_matrix;
        
        TH1D * hist_fidcorr;
        TH1D * hist_fake;

        RooUnfoldResponse response;
};

#endif //_UNFOLD_MANAGER_H_
